<?php
class Users_model extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	public function create_user( $post_data ){
		$data = array("status" => FALSE, "message" => "Something went wrong! Please reload the page and try again...");

		if(!empty($post_data)){
			$this->load->library('form_validation');

			$this->form_validation->set_rules('first_name', 'First Name', 'required');
			$this->form_validation->set_rules('last_name', 'Last Name', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'required|matches[password]');
            $this->form_validation->set_rules('email_address', 'Email', 'required');

          	if ($this->form_validation->run() == FALSE){
    			$data['message'] = validation_errors();
				$data['status'] = FALSE;
            }
            else{
            	/*Check if user is already existing on the database*/
            	$existing_user = $this->db->where("email_address", $post_data['email_address'])->get("users")->row_array();

            	var_dump($existing_user);

            	if(!empty($existing_user)){
	            	unset($post_data['confirm_password']);
	            	$post_data['created_at'] = date("Y-m-d H:i:s");

	            	/*Saving of users info*/
	            	$data['status'] =  $this->db->insert("users", $post_data);
	            	$user_id = $this->db->insert_id();

	            	$data['message'] = "User successfully registered.";
            	}
            	else{
            		$data['message'] = "Email address is already existing.";
            	}
            }
		}

		return $data;
	}

}
//eof