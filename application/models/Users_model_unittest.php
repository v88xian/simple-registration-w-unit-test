<?php
class Users_model_unittest extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	public function create_user( $post_data ){
		$data = array("status" => FALSE, "message" => "Something went wrong! Please reload the page and try again...");

		/*Running test to check if the post data is not empty*/
		$this->unit->run($post_data, "is_array", "Is post_data is array?", "The post data should should be in array form.");

		if(!empty($post_data)){
			$this->load->library('form_validation');
			$this->form_validation->set_data($post_data);

			$this->form_validation->set_rules('first_name', 'First Name', 'required');
			$this->form_validation->set_rules('last_name', 'Last Name', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'required|matches[password]');
            $this->form_validation->set_rules('email_address', 'Email', 'required');

            /*Running test cases*/
            /*Double checking if post data fields are not black*/
            $this->unit->run(!empty($post_data['first_name']), "is_true", "Is post_data['first_name'] is blank?", "The first name should not be empty.");
            $this->unit->run($post_data['first_name'], "is_string", "Is post_data['first_name'] is a string type?", "The first name should be a string type.");
            /*Running test cases*/
            $this->unit->run(!empty($post_data['last_name']), "is_true", "Is post_data['last_name'] is blank?", "The last name should not be empty.");
            $this->unit->run(!empty($post_data['password']), "is_true", "Is post_data['password'] is blank?", "The password should not be empty.");
            $this->unit->run(!empty($post_data['confirm_password']), "is_true", "Is post_data['confirm_password'] is blank?", "The password confirmation should not be empty.");
            $this->unit->run(!empty($post_data['email_address']), "is_true", "Is post_data['email_address'] is blank?", "The email_address confirmation should not be empty.");

          	if ($this->form_validation->run() == FALSE){
    			$data['message'] = validation_errors();
				$data['status'] = FALSE;
            }
            else{
            	/*Check if user is already existing on the database*/
            	$existing_user = $this->db->where("email_address", $post_data['email_address'])->get("users")->row_array();

            	/*Running test cases*/
            	$this->unit->run(!empty($existing_user), "is_true", "Is the email address entered is existing on the database?");
            	$this->unit->run($existing_user, "is_array", "Is existing_user is an array?");

            	if(empty($existing_user)){
	            	unset($post_data['confirm_password']);
	            	$post_data['created_at'] = date("Y-m-d H:i:s");

	            	/*Running test cases*/
	            	/*Checking if the confirm_password postdata field is successfully unset*/
	            	$this->unit->run(isset($post_data['confirm_password']), "is_false", "Is the confirm_password field is successfully unset?", "The confirm_password should be removed on the post_data");

	            	/*Saving of users info*/
	            	$data['status'] =  $this->db->insert("users", $post_data);
	            	$user_id = $this->db->insert_id();

	            	/*Running test cases*/
	            	$this->unit->run($data['status'], "is_true", "Is the user information is successfully saved on the database?", "Expected result should be 'passed'. ");
	            	$this->unit->run($user_id, "is_int", "Is the user ID is an integer?", "Expected result should be 'passed'. ");

	            	$data['message'] = "User successfully registered.";
            	}
            	else{
            		$data['message'] = "Email address is already existing.";
            	}
            }
		}

		/*Running test cases*/
		/*Checking if the data['status'] is TRUE*/
		$this->unit->run($data['status'], "is_true", "Is new user successfully created?");

		return $data;
	}

}
//eof