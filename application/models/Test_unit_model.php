<?php
class Test_unit_model extends CI_Model {
	
	public function __construct(){
		parent::__construct();
	}

	public function unit_test($model_name, $unit_test_function_name){
		/* Initialize unit test library*/
		$this->load->library('unit_test');

		/* Initialize/load the model */
		/* This is for the user if they forgot to upper case the first letter of model name */
		$model_name = ucfirst($model_name)."_unittest";
		$this->load->model($model_name);

		/* Fetch the post the depends on the model name, function name and some special name*/
		$post_data = $this->get_post_data($model_name, $unit_test_function_name);

		/* This is a test mode, 
		Note that NO data will be save in the database because it will be rolled back but still show the status of CRUD */
		$this->db->trans_start(TRUE);

		/* This will execute the model of unit test */
		$this->$model_name->$unit_test_function_name($post_data);
		
		/* Show the unit test report */

		var_dump($this->unit->result());

		/*You can customize the result on your own perspective*/
		// foreach($this->unit->result() as $key => $result) {
		// 	var_dump($result);
		// }
	}

	public function get_post_data($model_name, $unit_test_function_name){
		if($model_name == "Users_model_unittest"){
			if($unit_test_function_name == "create_user"){
				return array(
					"first_name" 		=> "My First Name",
					"last_name" 		=> "My Last Name",
					"email_address" 	=> "test@test.com",
					"password" 			=> "test",
					"confirm_password" 	=> "test"
				);
			}
		}
	}


}