<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit_test extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
	}

	public function run_test($model_name, $unit_test_function_name){
		$this->load->model("Test_unit_model");
		$this->Test_unit_model->unit_test($model_name, $unit_test_function_name);
	}
}