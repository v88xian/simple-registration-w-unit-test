<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function index(){
		$this->load->view('index');
	}

	public function registration(){
		$this->load->model("Users_model");
		$this->Users_model->create_user( $this->input->post() );
	}

}
