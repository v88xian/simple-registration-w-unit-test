<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Registration</title>
</head>
<body>

<div id="container">
	<h1>Welcome New User!</h1>

	<div id="body">
		<form action="/Users/registration" method="post" id="user_registration">
			<span>First Name: </span> <input type="text" name="first_name" id="user_first_name"> <br />
			<span>Last Name: </span> <input type="text" name="last_name" id="user_last_name"> <br />
			<span>Email Address: </span> <input type="email" name="email_address" id="user_email"> <br />
			<span>Password: </span> <input type="password" name="password" id="user_password"> <br />
			<span>Confirm Password: </span> <input type="password" name="confirm_password" id="user_cpassword"> <br />

			<input type="submit" value="Register!">
		</form>
	</div>
</div>

</body>
</html>